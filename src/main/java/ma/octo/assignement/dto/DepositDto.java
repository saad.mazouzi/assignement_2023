package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter @Setter
public class DepositDto {



    private BigDecimal montant;
    private String nomPrenomEmetteur;
    private String rib; //this will be used to find the accout
    private String motifDeposit;
    private Date dateExecution;
}
