package ma.octo.assignement.mapper;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;

public class DepositMapper {

    DepositMapper(){

    }
    public static Deposit mapDtoDeposit(DepositDto depositDto, Compte compteBeneficiaire) {


        Deposit deposit = new Deposit();
        deposit.setDateExecution(depositDto.getDateExecution());
        deposit.setCompteBeneficiaire(compteBeneficiaire);
        deposit.setMontant(depositDto.getMontant());
        deposit.setMotifDeposit(depositDto.getMotifDeposit());
        deposit.setNomPrenomEmetteur(depositDto.getNomPrenomEmetteur());


        return deposit;

    }
}
