package ma.octo.assignement.domain.util;


public enum EventType {

  TRANSFER("transfer"),
  DEPOSIT("Deposit d'argent");

  private final String  type;

  EventType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }



}
