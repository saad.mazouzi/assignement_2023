package ma.octo.assignement.config;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "ma.octo.assignement.repository")
public class JpaConfig {



    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("ma.octo.assignement.domain");
        em.setPersistenceUnitName("entityManager");

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());

        return em;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "create");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        return properties;
    }

    @Bean
    CommandLineRunner commandLineRunner(
            CompteRepository compteRepository, UtilisateurRepository utilisateurRepository, TransferRepository transferRepository) {
        return args -> {

            Utilisateur utilisateur1 = new Utilisateur();
            utilisateur1.setUsername("user1");
            utilisateur1.setLastname("last1");
            utilisateur1.setFirstname("first1");
            utilisateur1.setGender("Male");

            utilisateurRepository.save(utilisateur1);


            Utilisateur utilisateur2 = new Utilisateur();
            utilisateur2.setUsername("user2");
            utilisateur2.setLastname("last2");
            utilisateur2.setFirstname("first2");
            utilisateur2.setGender("Female");

            utilisateurRepository.save(utilisateur2);

            Compte compte1 = new Compte();
            compte1.setNrCompte("010000A000001000");
            compte1.setRib("RIB1");
            compte1.setSolde(BigDecimal.valueOf(200000L));
            compte1.setUtilisateur(utilisateur1);

            compteRepository.save(compte1);

            Compte compte2 = new Compte();
            compte2.setNrCompte("010000B025001000");
            compte2.setRib("RIB2");
            compte2.setSolde(BigDecimal.valueOf(140000L));
            compte2.setUtilisateur(utilisateur2);

            compteRepository.save(compte2);

            Transfer v = new Transfer();
            v.setMontantTransfer(BigDecimal.TEN);
            v.setCompteBeneficiaire(compte2);
            v.setCompteEmetteur(compte1);
            v.setDateExecution(new Date());
            v.setMotifTransfer("Assignment 2021");

            transferRepository.save(v);

        };
    }

}
