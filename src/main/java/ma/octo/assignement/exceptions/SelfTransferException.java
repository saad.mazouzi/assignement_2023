package ma.octo.assignement.exceptions;

public class SelfTransferException extends Exception{

    private static final long serialVersionUID = 1L;

    public SelfTransferException() {
    }

    public SelfTransferException(String message) {
        super(message);
    }
}

