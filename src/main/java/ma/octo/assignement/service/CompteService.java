package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Service
public class CompteService {
    Logger logger = LoggerFactory.getLogger(CompteService.class);

    private final CompteRepository compteRepository;

    @Autowired
    public CompteService(CompteRepository compteRepository) {
        this.compteRepository = compteRepository;
    }


    public List<Compte> loadAllCompte() {
        logger.info("Lister des Comptes");
        List<Compte> all = compteRepository.findAll();
        if (CollectionUtils.isEmpty(all)) {
            return Collections.emptyList();
        } else {
            return all;
        }
    }
}
