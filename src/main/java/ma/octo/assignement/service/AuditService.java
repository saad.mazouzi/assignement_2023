package ma.octo.assignement.service;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditService {

    Logger logger = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private AuditRepository auditRepository;

    public void audit(String message) {

        logger.info("Audit de l'événement {}", EventType.TRANSFER);

        Audit audit = new Audit();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage(message);
        auditRepository.save(audit);
    }



}
