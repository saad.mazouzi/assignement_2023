package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SelfTransferException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class TransferService {

    public static final int MONTANT_MAXIMAL = 10000;

    //All Repositories
    private final TransferRepository transferRepository;
    private final CompteRepository compteRepository;

    private final AuditService auditService;


    Logger logger = LoggerFactory.getLogger(TransferService.class);

    @Autowired
    public TransferService(
            TransferRepository transferRepository,
            CompteRepository compteRepository, AuditService auditService) {

        this.transferRepository = transferRepository;
        this.compteRepository = compteRepository;
        this.auditService = auditService;
    }

    //first API call "all transfers "
    public List<Transfer> loadAllTransfer() {
        logger.info("Lister des transfers");
        List<Transfer> all = transferRepository.findAll();
        if (CollectionUtils.isEmpty(all)) {
            return Collections.emptyList();
        } else {
            return all;
        }
    }



    //the transfer post api endpoint

    public void createTransaction(TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException,SelfTransferException {

        transferDto.setDate(new Date());
        //let's find the corresponding account for the transfer
        Compte compteEmeteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());


        compteExistDatabase(compteEmeteur,compteBeneficiaire);
        selfTransferVerification(compteEmeteur,compteBeneficiaire);

        montantVerification(transferDto.getMontant());

        motifExistanceVerification(transferDto.getMotif());

        soldSufisantVerification(compteEmeteur.getSolde().intValue() ,transferDto.getMontant().intValue() );



        //setting the sold of both the emeteur and the receiver
        setNewSoldForAccounts( compteEmeteur, compteBeneficiaire, transferDto);

        //saving the new transfer in the bd
        Transfer transfer = TransferMapper.mapDtoToTransfer(transferDto,compteEmeteur,compteBeneficiaire);
        transferRepository.save(transfer);

        //now for the auditing we use ; we use it to store the transfer audit
        auditService.audit("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString());
    }


    //sub functions for transefer
     void compteExistDatabase(Compte emetteur ,Compte benifiaire)throws CompteNonExistantException{
         if (emetteur == null || benifiaire == null ) {
             logger.info("Compte Non existant");
             throw new CompteNonExistantException("Compte Non existant");
         }

     }

     void selfTransferVerification(Compte emetteur ,Compte benifiaire)throws SelfTransferException{
        if(emetteur.getNrCompte().equals(benifiaire.getNrCompte())){
            throw new SelfTransferException("L'envoie de transaction au meme compte est impossible ");
        }
     }

     void montantVerification(BigDecimal montant) throws TransactionException {

         if (montant == null || montant.intValue() == 0) {
             logger.info("Montant vide");
             throw new TransactionException("Montant vide");
         }

          if (montant.intValue() < 10) {
              logger.info("Montant minimal de transfer non atteint");
             throw new TransactionException("Montant minimal de transfer non atteint");
         }

          if (montant.intValue() > MONTANT_MAXIMAL) {
              logger.info("Montant maximal de transfer dépassé");
             throw new TransactionException("Montant maximal de transfer dépassé");
         }


     }

     void motifExistanceVerification(String motif) throws TransactionException {
         if (motif.length() == 0) {
             logger.info("Motif vide");
             throw new TransactionException("Motif vide");
         }
     }

     void soldSufisantVerification(int solde , int transfetMotant ) throws SoldeDisponibleInsuffisantException {
         if (solde - transfetMotant < 0) {
             logger.error("Solde insuffisant pour l'utilisateur");
             throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
         }
     }

     void setNewSoldForAccounts(Compte compteEmeteur,Compte compteBeneficiaire, TransferDto transferDto){
         compteEmeteur.setSolde(
                 compteEmeteur.getSolde().subtract(transferDto.getMontant())
         );

         compteRepository.save(compteEmeteur);

         compteBeneficiaire.setSolde(
                 compteBeneficiaire.getSolde().add(transferDto.getMontant())
         );

         compteRepository.save(compteBeneficiaire);

     }

}
