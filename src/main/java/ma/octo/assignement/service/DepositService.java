package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class DepositService {

    public static final int MONTANT_MAXIMAL = 10000;

    private final DepositRepository depositRepository;
    private final CompteRepository compteRepository;

    private final AuditService auditService;

    Logger logger = LoggerFactory.getLogger(DepositService.class);

    @Autowired
    public DepositService(DepositRepository depositRepository, CompteRepository compteRepository, AuditService auditService) {
        this.depositRepository = depositRepository;
        this.compteRepository = compteRepository;
        this.auditService = auditService;
    }


    public void createDeposit(DepositDto depositDto)throws CompteNonExistantException,TransactionException {

        depositDto.setDateExecution(new Date());

        Compte compteBenificiaire = compteRepository.findByRib(depositDto.getRib());

        compteExistDatabase(compteBenificiaire);

        montantVerification(depositDto.getMontant());

        motifExistanceVerification(depositDto.getMotifDeposit());

        compteBenificiaire.setSolde(
                compteBenificiaire.getSolde().add(depositDto.getMontant())
        );
        compteRepository.save(compteBenificiaire);


        Deposit deposit = DepositMapper.mapDtoDeposit(depositDto,compteBenificiaire);
        depositRepository.save(deposit);

        auditService.audit("Deposit par " + depositDto.getNomPrenomEmetteur() + " vers " + deposit
                .getCompteBeneficiaire().getNrCompte() + " d'un montant de " + deposit.getMontant()
                .toString());



    }

    void compteExistDatabase(Compte benifiaire)throws CompteNonExistantException {
        if ( benifiaire == null ) {
            logger.info("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

    }

    void montantVerification(BigDecimal montant) throws TransactionException {

        if (montant == null || montant.intValue() == 0) {
            logger.info("Montant vide");
            throw new TransactionException("Montant vide");
        }

        if (montant.intValue() < 10) {
            logger.info("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        }

        if (montant.intValue() > MONTANT_MAXIMAL) {
            logger.info("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }


    }

    void motifExistanceVerification(String motif) throws TransactionException {
        if (motif.length() == 0) {
            logger.info("Motif vide");
            throw new TransactionException("Motif vide");
        }
    }

}
