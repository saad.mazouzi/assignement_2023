package ma.octo.assignement.web;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;

import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="api/v0/Deposit")
public class DepositController {

    public final DepositService depositService ;

@Autowired
    public DepositController(DepositService depositService) {
        this.depositService = depositService;
    }

    @PostMapping("executerDeposits")
    @ResponseStatus(HttpStatus.CREATED)
    public void createDeposit(@RequestBody DepositDto depositDto)
            throws  CompteNonExistantException, TransactionException {
        depositService.createDeposit(depositDto);
    }

}
