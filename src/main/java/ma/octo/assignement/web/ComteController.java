package ma.octo.assignement.web;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path="api/v0/Compte")

public class ComteController {

    public final CompteService compteService ;

    @Autowired
    public ComteController(CompteService compteService) {
        this.compteService = compteService;
    }

    @GetMapping("listOfAccounts")
    List<Compte> loadAllCompte() {
        return compteService.loadAllCompte();

    }

}
