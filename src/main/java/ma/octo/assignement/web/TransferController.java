package ma.octo.assignement.web;


import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SelfTransferException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(path="api/v0/transfers")
public class TransferController {


    public final TransferService transferService ;



    @Autowired
    TransferController(TransferService transferService) {
        this.transferService = transferService;
    }

    @GetMapping("listOfTransfers")
    List<Transfer> loadAllTransfer() {
      return transferService.loadAllTransfer();

    }



    @PostMapping("executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException , SelfTransferException {
       transferService.createTransaction(transferDto);
    }


}
