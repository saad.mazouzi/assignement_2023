package ma.octo.assignement.web;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.UtilasateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path="api/v0/Utilisateur")


public class UtilisateurController {


    public final UtilasateurService utilasateurService ;

    public UtilisateurController(UtilasateurService utilasateurService) {
        this.utilasateurService = utilasateurService;
    }

    @Autowired

    @GetMapping("listOfUtilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        return utilasateurService.loadAllUtilisateur();

    }

}
