package ma.octo.assignement.service;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.jupiter.api.Assertions.*;
import java.math.BigDecimal;

class DepositServiceTest {

    private final  DepositService depositService;

    @Autowired
    DepositServiceTest(DepositService depositService) {
        this.depositService = depositService;

    }

    @Test
    void createDepositWrongRib() {


        DepositDto depositDto = new DepositDto();
        depositDto.setRib("AD09877655");
        depositDto.setMontant(BigDecimal.valueOf(1234));
        depositDto.setNomPrenomEmetteur("saad");

        assertThrows(CompteNonExistantException.class,
                ()-> {
                    depositService.createDeposit(depositDto);
                }
                );



    }
}